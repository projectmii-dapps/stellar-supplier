let Queue = require('promise-queue');

const productProperties = ['txid', 'item', 'product', 'lot', 'provenance', 'quantity', 'type', 'action']
const products = 'productTable';
const columns = [
	{ id: productProperties[0], text: 'txid', tag: 'input', type: 'text', value: '', hidden: true },
	{ id: productProperties[1], text: 'Cod. Art.', tag: 'input', type: 'text', value: '', hidden: false },
	{ id: productProperties[2], text: 'Prodotto', tag: 'input', type: 'text', value: '', hidden: false },
	{ id: productProperties[3], text: 'Lotto', tag: 'input', type: 'text', value: '', hidden: false },
	// { id: productProperties[4], text: 'Provenienza', tag: 'input', type: 'text', value: '', hidden: false },
	{ id: productProperties[5], text: 'Qnt. (kg)', tag: 'input', type: 'number', value: '', hidden: false },
	{ id: productProperties[6], text: 'Tipo', tag: 'select', type: '', value: '', hidden: false },
	{ id: productProperties[productProperties.length - 1], text: 'Rimuovi', tag: 'input', type: 'button', value: '-', hidden: false }
]
const options = ['Ingrediente', 'Spezie/Aromi/Additivi', 'Verdure/Ortaggi/Creme',
	'Salumi/Formaggi/Altro', 'Art. Confezionamento'];

const txProperties = ['count', 'memo'];
const transactions = 'txTable';
const txColumns = [
	{ id: txProperties[0], text: 'Nr', width: 'width: 10%', align: 'center'},
	{ id: txProperties[1], text: 'Hash', width: '', align: 'left'}
]

const productsAddr = 'GBJCYYE4CYT2Y4KD67XYD75VGXROWPP6VE4CZPWE2L3ZEJNSMVKAEJUL';
const supplyAddr = 'GDA4CN76BLEBX2R6DNL47GXXJHMHTYNIECUBEIISL2BA34K4CBDXDDKW';
// supply priv 	SBHK7N3FSMWIZDISUOQXVT2KZRLZAUFYDMHPFCGIQCJXIS6NPAFDBCQ5

const wasteAddr = 'GAQJLX2K7ZSTXYDAZBL3D5EDZGPNT6STMUAA5PVAKPAY73LAHGDMFRRX';
// priv SBR6TZ4NX2RVAE5BDGK7XE5V77OU4756HSNC4PEHJDL6I6EE4T3LS377

const supplierAddr = 'GCTZIQ3N245UK3V2CP6YMVR4K6SPBKJ4DQKLGQCQ5POCFEX2KUNQ7T3F';
// priv SAOKMZS7IX65WCOHTSUUTDSYN2KHTYZ6LZ3US2YPMLG2AYKJEZHWQCRR

window.invoice = {
	safe: true,
	actionQueue: null,

	products: [],
	productsQueue: null,

	consumedProducts: [],
	consumedProductsQueue: null,

	start: function() {
		this.actionQueue = new Queue(1);
		this.productsQueue = new Queue();
		this.consumedProductsQueue = new Queue();

		this.buildHeader();
		// this.buildTxHeader();
		this.getTransactions();
		this.getSuppliers();
	},

	buildHeader: function() {
		let table = document.getElementById(products);
		let tableLength = table.rows.length;

		while (table.rows.length > 0) { table.deleteRow(-1); }

		let header = table.createTHead();
		let hRow = header.insertRow(0);

		for (let i = 0; i < columns.length; i++) {
			let hCell = hRow.insertCell(-1);
			hCell.id = columns[i].id;
			hCell.hidden = columns[i].hidden;
			hCell.innerHTML = columns[i].text;
			hCell.style = 'font-weight: bold;';
			hCell.align = 'center';
			if (columns[i].id === 'product') {
				hCell.style = 'font-weight: bold; width: 35%;';
			}
		}
	},

	onLedgerConfirmation: function(data) {
		let table = document.getElementById(transactions);
		let row = table.insertRow(1);

		let cell = row.insertCell(-1);
		cell.innerHTML = table.rows.length - 1;
		cell.align = 'center';

		cell = row.insertCell(-1);
		cell.innerHTML = data.hash;
		cell.align = 'right';
	},

	addRow: function() {
		let self = this;

		let table = document.getElementById(products);
		let newRow = table.insertRow(-1);

		console.log('cells:', table.rows[0].cells.length);
		for (let i = 0; i < table.rows[0].cells.length; i++) {
			let newCell = newRow.insertCell(-1);
			let input = document.createElement(columns[i].tag);

			newCell.headers = columns[i].id;
			newCell.hidden = columns[i].hidden;
			input.type = columns[i].type;
			input.value = columns[i].value;
			input.style = 'width: 99%;';

			if (newCell.headers == productProperties[productProperties.length - 1]) {
				input.onclick = function() {
					self.deleteRow(newRow.rowIndex);
				};
			}

			newCell.appendChild(input);
			newCell.align = 'center';
		}

		options.map(function(text) {
			let option = document.createElement("option");
			option.text = text;
			newRow.cells[5].children[0].add(option);
		})

		return newRow;
	},

	deleteRow: function(rowNumber) {
		let productTable = document.getElementById(products);
		productTable.deleteRow(rowNumber);
	},

	extract: function() {
		let productTable = document.getElementById(products);
		let invoiceData = {};

		invoiceData['date'] = document.getElementById('invoiceDate').value;
		invoiceData['count'] = document.getElementById('invoiceNumber').value;
		invoiceData['description'] = document.getElementById('invoiceDescr').value;

		let sender = {};
		sender['name'] = document.getElementById('supplier').value;
		sender['address'] = document.getElementById('supplierAddress').value;
		sender['zip'] = document.getElementById('supplierZip').value;
		sender['city'] = document.getElementById('supplierCity').value;
		sender['province'] = document.getElementById('supplierProvince').value;
		sender['state'] = document.getElementById('supplierState').value;
		sender['vat'] = document.getElementById('supplierVat').value;
		sender['fiscalcode'] = document.getElementById('supplierFc').value;
		sender['site'] = document.getElementById('supplierSite').value;
		sender['eec'] = document.getElementById('supplierEec').value;
		invoiceData['sender'] = sender;

		let receiver = {};
		receiver['name'] = document.getElementById('receiver').value;
		receiver['address'] = document.getElementById('receiverAddress').value;
		receiver['zip'] = document.getElementById('receiverZip').value;
		receiver['city'] = document.getElementById('receiverCity').value;
		receiver['province'] = document.getElementById('receiverProvince').value;
		receiver['state'] = document.getElementById('receiverState').value;
		receiver['vat'] = document.getElementById('receiverVat').value;
		receiver['fiscalcode'] = document.getElementById('receiverFc').value;
		receiver['site'] = document.getElementById('receiverSite').value;
		receiver['eec'] = document.getElementById('receiverEec').value;
		invoiceData['receiver'] = receiver;

		invoiceData['products'] = [];
		for (let i = 1; i < productTable.rows.length; i++) {
			let row = productTable.rows[i];
			let product = {};

			for (let j = 0; j < productProperties.length - 1; j++) {
				let cell = row.cells[j];
				let cellHeader = cell.headers;

				if (cell.children[0]) {
					product[cellHeader] = cell.childNodes[0].value;
				} else {
					console.log(cellHeader, cell.innerHTML);
					product[cellHeader] = cell.innerHTML;
				}
			}

			invoiceData.products.push(product);
		}
		console.log(invoiceData);
		let productsJson = JSON.stringify(invoiceData);
		storage.addFile(productsJson, this.onInvoiceUpload);

		// let supplierJson = JSON.stringify(supplier);
		invoice.addSupplier(sender);

		return invoiceData;
	},

	getSuppliers: function() {
		ledger.getTxs(supplierAddr)
		.then(page => {
			if (page.records.length > 0) {
				invoice.onTxRecords(invoice.products, invoice.productsQueue, page,
					[ledger.filterDataTxs,
						invoice.transformSupMemos]);
			}
		});
	},

	addSupplier: function(supData) {
		let table = document.getElementById('suppliers');
		let isKnown = false;

		for (let i = 1; i < table.rows.length; i++) {
			console.log('is', table.rows[i].cells[2].innerHTML, 'equal to', supData.name);
			if (table.rows[i].cells[2].innerHTML == supData.name) {
				isKnown = true;
				break;
			}
		}

		if (!isKnown) {
			storage.addFile(JSON.stringify(supData), this.onSupplierUpload);
		}
	},

	displaySuppliers: function(txs, files) {
		let table = document.getElementById('suppliers');

		files.forEach(function(file, index) {
			let supplier = JSON.parse(file);
			let row = table.insertRow(-1);

			let hiddenCell = row.insertCell(-1);
			hiddenCell.innerHTML = file;
			hiddenCell.hidden = true;

			let input = document.createElement('input');
			input.type = 'button';
			input.value = 'Scegli';
			input.style = 'width: 100%;';
			input.onclick = function() {
				document.getElementById('supplier').value = supplier.name;
				document.getElementById('supplierAddress').value = supplier.address;
				document.getElementById('supplierZip').value = supplier.zip;
				document.getElementById('supplierCity').value = supplier.city;
				document.getElementById('supplierProvince').value = supplier.province;
				document.getElementById('supplierState').value = supplier.state;
				document.getElementById('supplierVat').value = supplier.vat;
				document.getElementById('supplierFc').value = supplier.fiscalcode;
				document.getElementById('supplierSite').value = supplier.site;
				document.getElementById('supplierEec').value = supplier.eec;
			}
			let cell = row.insertCell(-1).appendChild(input);
			cell.align = 'center';

			cell = row.insertCell(-1);
			cell.innerHTML = supplier.name;
			cell.align = 'center';
			console.log(supplier);
			row.insertCell(-1).innerHTML = supplier.vat;

			cell = row.insertCell(-1);
			cell.innerHTML = supplier.eec;
			cell.align = 'center';
		});
	},

	transformSupMemos: function(txs) {
		let getHex = function(tx) {
			return new Promise(function(resolve) {
				resolve(utils.fromBase64toHex(tx.memo));
			})
		}

		let getMultiHash = function(hexStr) {
			return new Promise(function(resolve) {
				resolve(utils.fromHexToBase58(hexStr));
			});
		}

		Promise.all(txs.map(getHex))
		.then(hexStrs => Promise.all(hexStrs.map(getMultiHash)))
		.then(multiHashes => storage.getChunks(multiHashes,
			invoice.displaySuppliers,
			txs));
	},

	onSupplierUpload: async function(content, supMultiHash) {
		let mainSup = JSON.parse(content);
		let hash = utils.fromBase58toHex(supMultiHash);

		invoice.actionQueue.add(function() {
			invoice.safe = false;

			let x = document.getElementById("snackbar");
			x.className = "show";

			return ledger.submitMemoHashTx(hash, supplierAddr);
		})
		.then(() => { invoice.checkQueue(invoice.actionQueue); });
	},

	onInvoiceUpload: async function(content, invMultiHash) {
		let mainInvoice = JSON.parse(content);

		let products = await Promise.all(mainInvoice.products.map(product => {
			product['invoiceMultiHash'] = invMultiHash;
			return JSON.stringify(product);
		}));

		let getMultiHash = (product) => {
			return new Promise(function(resolve) {
				resolve(storage.addFilePromise(product));
			});
		};

		let getHash = (multiHash) => {
			return new Promise(function(resolve) {
				resolve(utils.fromBase58toHex(multiHash));
			});
		};

		Promise.all(products.map(getMultiHash))
		.then(multiHashes => Promise.all(multiHashes.map(getHash)))
		.then(hashes => {
			invoice.buildHeader();
			hashes.map(hash => {
				invoice.actionQueue.add(function() {
					invoice.safe = false;

					let x = document.getElementById("snackbar");
					x.className = "show";

					// invoice.onLedgerConfirmation
					return ledger.submitMemoHashTx(hash, supplyAddr);
				})
				.then(() => { invoice.checkQueue(invoice.actionQueue); });
			});
		});
	},

	checkQueue: function(promiQueue) {
		if (promiQueue.getQueueLength() === 0
			&& promiQueue.getPendingLength() === 0)
		{
			this.safe = true;
			let x = document.getElementById("snackbar");
			x.className = x.className.replace("show", "");
		}
	},

	getAvailableProducts: function(txs) {
		/* TO DO: eveluate efficiency */
		// _.difference(products, _.intersection());
		let consumedHashes = invoice.consumedProducts.map(tx => { return tx.memo; });
		return txs.filter(tx => consumedHashes.indexOf(tx.memo) == -1);
	},

	killProduct: function(rowIndex) {
		let table = document.getElementById('availableComponentsTable');
		let multiHash = table.rows[rowIndex].cells[0].innerHTML;
		let hash = utils.fromBase64toHex(multiHash);

		table.deleteRow(rowIndex);
		this.actionQueue.add(function() {
			this.safe = false;
			let x = document.getElementById("snackbar");
			x.className = "show";
			return ledger.submitMemoHashTx(hash, wasteAddr)
		}).then(() => { invoice.checkQueue(invoice.actionQueue); });
	},

	displayProducts: function(txs, files) {
		let table = document.getElementById('availableComponentsTable');

		files.forEach(function(file, index) {
			let product = JSON.parse(file);
			console.log(product);
			let row = table.insertRow(-1);

			let hiddenCell = row.insertCell(-1);
			console.log(txs[index].id);
			hiddenCell.innerHTML = txs[index].memo;
			hiddenCell.hidden = true;

			hiddenCell = row.insertCell(-1);
			// hiddenCell.innerHTML = multiHashes[index];
			hiddenCell.innerHTML = txs[index].id;
			hiddenCell.hidden = true;

			let input = document.createElement('input');
			input.type = 'button';
			input.value = '+';
			input.style = 'width: 100%;';
			input.onclick = function() {
				invoice.moveToIngredient(row);
				row.hidden = true;
			}
			let cell = row.insertCell(-1).appendChild(input);
			cell.align = 'center';

			cell = row.insertCell(-1);
			cell.innerHTML = product.productCode;
			cell.align = 'center';
			row.insertCell(-1).innerHTML = product.product;

			cell = row.insertCell(-1);
			cell.innerHTML = product.productLot;
			cell.align = 'center';
			row.insertCell(-1).innerHTML = product.quantity;

			input = document.createElement('input');
			input.type = 'button';
			input.value = 'Scarta';
			input.style = 'width: 100%;';
			input.onclick = function() {
				invoice.killProduct(row.rowIndex);
			};
			row.insertCell(-1).appendChild(input);
		});
	},

	transformMemos: function(txs) {
		let getHex = function(tx) {
			return new Promise(function(resolve) {
				resolve(utils.fromBase64toHex(tx.memo));
			})
		}

		let getMultiHash = function(hexStr) {
			return new Promise(function(resolve) {
				resolve(utils.fromHexToBase58(hexStr));
			});
		}

		Promise.all(txs.map(getHex))
		.then(hexStrs => Promise.all(hexStrs.map(getMultiHash)))
		.then(multiHashes => storage.getChunks(multiHashes,
			invoice.displayProducts,
			txs));
	},

	onTxRecords: function(array, promiseQueue, prevPage, criteria) {
		if (prevPage.records.length > 0) {
			promiseQueue.add(function() { return prevPage.next(); })
			.then(page => invoice.onTxRecords(array, promiseQueue, page, criteria));

			let txs = prevPage.records;
			if (Array.isArray(txs)) {
				if (typeof(criteria[0]) == 'function') {
					for (let i = 0; i < criteria.length; i++) {
						txs = criteria[i](txs);
					}
				}
			}
		}
	},

	getTransactions: function() {
		let self = this;

		ledger.getTxs(wasteAddr)
		.then(page => {
			// on waste txs select only the ones incoming from main account
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			if (page.records.length > 0) {
				self.onTxRecords(self.consumedProducts, self.consumedProductsQueue,
					page, [ledger.filterOutputTxs, ledger.filterDataTxs]);
			}

			ledger.getTxs('GBJCYYE4CYT2Y4KD67XYD75VGXROWPP6VE4CZPWE2L3ZEJNSMVKAEJUL')
			.then(page => {
				if (page.records.length > 0) {
					self.onTxRecords(self.products, self.productsQueue, page,
						[ledger.filterInputTxs,
						ledger.filterDataTxs,
						self.getAvailableProducts,
						self.transformMemos]);
				}
			});
		});
	},

	moveToIngredient: function(row) {
		let table = document.getElementById('productTable');
		let newRow = this.addRow();

		for (let i = 0; i < newRow.cells.length - 3; i++) {
			newRow.cells[i].removeChild(newRow.cells[i].childNodes[0]);
		}
		newRow.cells[0].innerHTML = row.cells[1].innerHTML;
		newRow.cells[1].innerHTML = row.cells[3].innerHTML;
		newRow.cells[2].innerHTML = row.cells[4].innerHTML;
		newRow.cells[3].innerHTML = row.cells[5].innerHTML;

		let input = newRow.cells[6].children[0];
		input.type = 'button';
		input.style = 'width: 99%;';
		input.value = '-';
		input.onclick = function() {
			document.getElementById('productTable').deleteRow(newRow.rowIndex);
			row.hidden = false;
		}
	},

	print: function() {
		window.print();
	}
}


window.addEventListener('load', function() {
	invoice.start();
});
