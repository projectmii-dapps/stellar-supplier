const ipfsAPI = require('ipfs-api');
const ipfs = ipfsAPI({ host: 'ec2-34-211-225-35.us-west-2.compute.amazonaws.com', port: 5001, protocol: 'http' })
//const ipfs = ipfsAPI({ host: 'ec2-34-211-225-35.us-west-2.compute.amazonaws.com', porot: 5001, protocol: 'http' });
// var ipfs = ipfsAPI('/ip4/127.0.0.1/tcp/5001');
// var ipfs = ipfsAPI('http://ec2-34-211-225-35.us-west-2.compute.amazonaws.com:5001');

const MAX_REQUESTS = 50;
const REQ_INTERVAL = 1; // ms

window.storage = {
	start: function() {},

	addFile: async function(content, onSuccess) {
		let buff = Buffer.from(content);
		let multiHash;

		ipfs.files.add(buff, { pin: true }).then(function(files) {
			multiHash = files[0].hash;
			if (onSuccess != undefined)
				onSuccess(content, multiHash);
		});
	},

	getFile: function(multiHash) {
		return ipfs.files.cat(multiHash);
	},

	addFilePromise: function(content) {
		let buff = new Buffer(content);
		return ipfs.files.add(buff, { pin: true }).then(function(files) {
			return files[0].hash; // returns content multhash
		});
	},

	getChunks: function(multiHashPipe, callback, clbk_param) {
		let mHashes = multiHashPipe.splice(-MAX_REQUESTS, MAX_REQUESTS);
		let promiseFile = function(multiHash) {
			return new Promise(function(resolve) {
				resolve(storage.getFile(multiHash));
			})
		}

		Promise.all(mHashes.map(promiseFile))
		.then(files => {
			if (typeof(callback) == 'function') {
				console.log(clbk_param);
				callback(clbk_param, files);
			}
			if (multiHashPipe.length > 0) {
				setTimeout(function() {
					storage.getChunks(multiHashPipe, callback);
				}, REQ_INTERVAL);
			}
		});
	}
}


window.addEventListener('load', function() {
	storage.start();
})
