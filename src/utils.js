import bs58 from 'bs58';

// 0x12=sha2 size:0x20=256
const HEX_PREFIX = '1220';

window.utils = {
	fromBase58toHex: function(multiHash) {
		console.log(multiHash);
		return bs58.decode(multiHash).slice(2).toString('hex');
	},

	fromBase64toHex: function(base64Str) {
		console.log('base64', base64Str);
		return Buffer.from(base64Str, 'base64').toString('hex');
	},

	fromHexToBase58: function(hexStr) {
		hexStr = HEX_PREFIX + hexStr;
		return bs58.encode(Buffer.from(hexStr, 'hex'));
	}
}
