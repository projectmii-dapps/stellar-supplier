import "./style.css";

if (process.env.NODE_ENV !== 'production') {
	console.log('DEVELOPMENT MODE');
}

const IPFS = require('ipfs-api');
const ipfs = new IPFS({ host: 'ipfs.infura.io',
	port: 5001,
	protocol: 'https' });

import bs58 from 'bs58';

let request = require('request');
let crypto = require('crypto');
let StellarSdk = require('stellar-sdk');

let horizonTestnet = 'https://horizon-testnet.stellar.org';
let horizonTestnetFriendbot = 'https://horizon-testnet.stellar.org/friendbot';
let server = new StellarSdk.Server(horizonTestnet);

let pub = 'GDKKLOMGRKNM7LPA57VQB2YVIYAMWMTKYXROSEUN2SJ4ZRRIQJQM6WQE';
let priv = 'SA2B2CJAKHPH42ADUAVTS2WPXU7QCUDBST6QPY7LZ7ODWEOILCKSK3JZ';

const MIN = '0.0000001';
const TX_CHUNK_SIZE = 200;


window.ledger = {
	start: function() {
		StellarSdk.Network.useTestNetwork();
	},

	filterInputTxs: function(txs, account) {
		if (account != undefined) account = pub;
		return txs.filter(tx => tx.source_account != account);
	},

	filterOutputTxs: function(txs) {
		return txs.filter(tx => tx.source_account == pub);
	},

	filterDataTxs: function(txs) {
		return txs.filter(tx => tx.memo_type == 'hash');
	},

	submitMemoHashTx: async function(hash, receiver, callback) {
		let issuerPair = StellarSdk.Keypair.fromSecret(priv);

		let transaction;
		await server.loadAccount(pub).then(function(account) {
			transaction = new StellarSdk.TransactionBuilder(account)
			.addOperation(StellarSdk.Operation.payment({
				destination: receiver,
				asset: StellarSdk.Asset.native(),
				amount: MIN,
			}))
			.addMemo(StellarSdk.Memo.hash(hash))
			.build();
			transaction.sign(issuerPair);

			console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));

			return account;
		})
		.catch(function(e) {
			console.error(e);
		});

		return server.submitTransaction(transaction)
		.then(function(transactionResult) {
			if (typeof(callback) == 'function') {
				callback(transactionResult);
			}
			else {
				console.log(typeof(callback), callback);
			}
			console.log(JSON.stringify(transactionResult, null, 2));
			console.log('\nSuccess! View the transaction at: ');
			console.log(transactionResult._links.transaction.href);
		})
		.catch(function(err) {
			console.log('An error has occured:');
			console.log(err);
		});
	},

	submitTxPipe: async function(pipe, onConfirmation, receiver) {
		let self = this;
		let issuerPair = StellarSdk.Keypair.fromSecret(invoiceIssuerPriv);

		server.loadAccount(invoiceIssuerPub).then(function(account) {
			let transaction = new StellarSdk.TransactionBuilder(account)
			.addOperation(StellarSdk.Operation.payment({
				destination: receiver,
				asset: StellarSdk.Asset.native(),
				amount: MIN,
			}))
			.addMemo(StellarSdk.Memo.hash(Buffer.from(pipe.pop(), 'hex')))
			.build();
			transaction.sign(issuerPair);

			console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));

			server.submitTransaction(transaction)
			.then(function(transactionResult) {
				if (typeof(onConfirmation) == 'function') {
					onConfirmation(transactionResult);
				}
				else {
					console.log(typeof(onConfirmation), onConfirmation);
				}

				if (pipe.length > 0) {
					self.submitTxPipe(pipe, onConfirmation);
				}

				console.log(JSON.stringify(transactionResult, null, 2));
				console.log('\nSuccess! View the transaction at: ');
				console.log(transactionResult._links.transaction.href);

				return transactionResult;
			})
			.catch(function(err) {
				console.log('An error has occured:');
				console.log(err);
			});

			return account;
		})
		.catch(function(e) {
			console.error(e);
		});
	},

	getTxs: function(account) {
		return server.transactions().forAccount(account)
		.limit(TX_CHUNK_SIZE).order('desc').call()
		.then(page => { return page; });
	},
}

window.addEventListener('load', function() {
	ledger.start();
});

// submitMemoHashTx: async function(hashes, onConfirmation) {
// 	let issuerPair = StellarSdk.Keypair.fromSecret(invoiceIssuerPriv);
//
// 	server.loadAccount(invoiceIssuerPub).then(function(account) {
// 		let transaction = new StellarSdk.TransactionBuilder(account)
// 		.addOperation(StellarSdk.Operation.payment({
// 			destination: supply,
// 			asset: StellarSdk.Asset.native(),
// 			amount: MIN,
// 		}))
// 		.addMemo(StellarSdk.Memo.hash(hashes))
// 		.build();
// 		transaction.sign(issuerPair);
//
// 		// console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));
//
// 		server.submitTransaction(transaction)
// 		.then(function(transactionResult) {
// 			onConfirmation(transactionResult);
// 			console.log(JSON.stringify(transactionResult, null, 2));
// 			console.log('\nSuccess! View the transaction at: ');
// 			console.log(transactionResult._links.transaction.href);
// 		})
// 		.catch(function(err) {
// 			console.log('An error has occured:');
// 			console.log(err);
// 		});
//
// 		return account;
// 	})
// 	.catch(function(e) {
// 		console.error(e);
// 	});
// },

// let es = server.transactions().forAccount(invoiceIssuerPub)
// .cursor('now')
// .stream({
// 		onmessage: function (message) {
// 			console.log(message);
// 	}
// })

// getInvoices: function() {
// 	server.transactions().forAccount(invoiceIssuerPub).call()
// 	.then(async function (page) {
// 		supplyHistory.push(...page.records);
// 		while (page.records.length > 0) {
// 			page = await page.next();
// 			supplyHistory.push(...page.records);
// 		}
// 	})
// 	.catch(function (err) { console.log(err); });
// }
